from gearbox.cogs.templates import Pastebin
from gearbox.models import PasteLink, Paste


class PastebinFetch(Pastebin):
    collects = PasteLink
    emits = Paste

    def receive(self, payload: PasteLink) -> None:
        try:
            paste = self.get_paste(payload.id)
        except Exception as e:
            self.log.exception(f"Failed to get paste: {payload.id}",
                                exc_info=e)
            return
        if not paste:
            self.log.info("No paste retreived")
            return
        if paste.get("error"):
            if "Please slow down" in paste["error"]:
                self.log.error(f"Rate limit exceeded, quitting module.")
                return
            self.log.error(f"Failed to get paste: {payload.id}: "
                            f"{paste['error']}")
            return
        for field in ("scrape_url", "full_url", "size", "hits"):
            del paste[field]
        self.emit(Paste(**paste))
        self.log.info(f"Retrieved paste {payload.id}")
