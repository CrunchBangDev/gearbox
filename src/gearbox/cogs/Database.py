from gearbox.cogs.templates import Cog
from gearbox.models import CogMemory
from gearbox.utils import hash_message
from pubsub import pub


class Database(Cog):
    schedule = False
    collects = pub.ALL_TOPICS
    def receive(self, payload):
        payload.save()
        self.log.debug(f"{payload} saved")

    def _receive(self, *args, **kwargs):
        """ Override to present more generic interface to pubsub """
        message_id = hash_message(kwargs["payload"])
        if message_id in self.messages_seen:
            return None
        self.receive(kwargs["payload"])
        self.messages_seen.append(message_id)
        CogMemory(cog=self.__class__.__name__, message_id = message_id).save()
