from json.decoder import JSONDecodeError

from requests import request
from gearbox.cogs.templates.Cog import Cog  # type: ignore
from gearbox.utils import merge  # type: ignore

module_config = ["url"]

class APIClient(Cog):
    schedule = 30000
    def __init__(self, *args, config=module_config, **kwargs):
        super().__init__(*args, config=merge(config, module_config), **kwargs)

    def request(self, uri, method="get", *args, **kwargs):
        return self._json_request(uri, method, *args, **kwargs)

    def _json_request(self, *args, **kwargs):
        res = self._request(*args, **kwargs)
        if res:
            try:
                return res.json()
            except JSONDecodeError as e:
                self.log.exception("JSON request failed:\n"
                                   f"  URL: {self.config['url']}/{args[0]}\n",
                                   exc_info=e)
        else:
            self.log.warning("Received None-type response")
        return

    def _request(self, uri, method, *args, **kwargs):
        try:
            res = request(method, f"{self.config['url']}/{uri}", *args, **kwargs)
            res.raise_for_status()
            return res
        except Exception as e:
            self.log.exception(f"Request failed", exc_info=e)

    def get(self, *args, **kwargs):
        return self.request(*args, method="get", **kwargs)

    def post(self, *args, **kwargs):
        return self.request(*args, method="post", **kwargs)

    def put(self, *args, **kwargs):
        return self.request(*args, method="put", **kwargs)

    def delete(self, *args, **kwargs):
        return self.request(*args, method="delete", **kwargs)
