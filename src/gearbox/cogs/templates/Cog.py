from collections import defaultdict
from typing import Any, Dict, List, Optional

from django.db.models.base import ModelBase
from pubsub import pub

import yaml
from .template import Template
from gearbox.utils import hash_message
from gearbox.models import CogMemory


class Cog(Template):
    collects, emits = None, None
    history = {}
    messages_seen = []
    def __init__(self, *args, key: str = "", config: list = None, **kwargs):
        """
        Performs initial configuration and binds data to cogs
        """
        super().__init__(*args, **kwargs)
        config = config or []
        self.config_keys: List[str] = config
        self.name = self.__class__.__name__
        self.statistics: Dict[str, Any] = {}
        self.history = {}
        self.messages_seen = []

    def setup(self, *args, **kwargs):
        print("Setting up")
        if not super().setup(*args, **kwargs):
            return False
        if hasattr(self, "state_model"):
            for record in self.state_model.values():
                self.history[record.cog] = record
        else:
            self.messages_seen = \
                CogMemory.objects.filter(cog=self.name).values_list('message_id', flat=True)

    def main(self, *args, **kwargs):
        """ Publisher cogs should override this method """
        pass

    def receive(self, payload):
        """ Subscriber cogs should override this method """
        self.log.debug(f"No handler for message: {payload}")
    
    def _receive(self, payload):
        """
        Wrapper for receive that filters out subscribed items that have already
        been received, e.g. during a previous run.
        """
        message_id = hash_message(payload)
        if message_id in self.messages_seen:
            return None
        self.receive(payload)
        self.messages_seen.append(message_id)
        CogMemory(cog=self.__class__.__name__, message_id = message_id).save()

    def setup(self, *args, **kwargs) -> bool:
        # This will cause early termination when in test mode
        if not super().setup(*args, **kwargs):
            return False

        if hasattr(self, "config") and isinstance(self.config, dict) and self.config:
            self.log.warn("Can't setup cog twice!")
            return False
        
        if self.collects is not None:
            if isinstance(self.collects, tuple):
                for topic in self.collects:
                    self.subscribe(topic)
            else:
                self.subscribe(self.collects)

        # Get cog-specific configuration values
        self.configure(self.config_keys)

        # Check for errors
        if self.config.get("error"):
            return False

        return True

    def subscribe(self, topic: str):
        if isinstance(topic, ModelBase):
            topic = topic().__class__.__name__.lower()
        elif topic is not pub.ALL_TOPICS:
            self.log.error(f"Attempted to subscribe to undefined topic: {topic}")
            return
        pub.subscribe(self._receive, topic)
        self.log.debug(f"Subscribed to {topic}")

    def configure(self, desired_config: list) -> None:
        """
        Turns a list of configuration keys into a config dictionary with the
        appropriate values, including looking up secret values using available meta
        cogs.

        Class inheritance is used to fill in unpopulated values with any available
        configuration elements from parent class(es).

        Parameters:
            desired_config (list): A list of keys to get configuration values for
        """
        config_file = "cog_config.yml"

        base_config = {}
        try:
            base_config = yaml.safe_load(open(config_file))
        except IOError:
            open(config_file, "w").close()
        base_config = base_config or {}

        self.config = {}
        class_bases = set(type.mro(self.__class__))
        for base in class_bases:
            base_class = base.__name__.replace("Cog", "")
            if not base_class or base_class == "object" or "Model" in base_class:
                continue
            # Operate on copy to avoid altering the basis for the loop
            for item in desired_config.copy():
                item_string = f"{base_class}_{item}"
                found_item = base_config.get(item_string, None)
                if found_item:
                    # Check for secrets
                    if self.is_secret_item(found_item):
                        secret_value = self.read_secret(found_item[7:-1])
                        if secret_value:
                            self.config[item] = secret_value
                            desired_config.remove(item)
                    # Append normal config item
                    else:
                        self.config[item] = base_config.get(item_string)
                        desired_config.remove(item)
        # Check to ensure all config options have been defined
        if len(desired_config) != 0:
            self.config["error"] = \
                    f"Missing config element(s): {', '.join(desired_config)}"

    def is_secret_item(self, item):
        return item and type(item) is str and \
                item[0:7] == "secret(" and item[-1] == ")"

    def read_secret(self, location: str) -> Optional[Any]:
        """
        Read a secret from available secrets cogs

        parameter:
            location (str): Secret identifier to be passed to secrets providers
        """
        if self.secrets():
            secret_value = str()
            for secret_cog in self.secrets():
                secret_value = secret_cog.read(location)
                if secret_value:
                    return secret_value
        else:
            self.log.warn(f"No secrets cogs available")
        return False


    def get_meta(self, meta_type: str) -> list:
        """
        Searches the class' meta dictionary for adapters of a given type

        Parameters:
            meta_type (str): The adapter type to fetch
        """
        try:
            return list(self.meta_cogs[meta_type])
        except KeyError:
            return list()

    def secrets(self) -> list:
        return self.get_meta("Secrets")

    def manager(self) -> list:
        return self.get_meta("Manager")

    def emit(self, data):
        topic_name = data.__class__.__name__.lower()
        pub.sendMessage(topicName=topic_name, payload=data)
