import re
from json.decoder import JSONDecodeError
from typing import Optional

from . import APIClient  # type: ignore
from gearbox.utils import merge  # type: ignore

module_config = ["api_key"]

class Pastebin(APIClient):
    schedule = False
    def __init__(self, *args, config=module_config, **kwargs):
        super().__init__(*args, config=merge(config, module_config), **kwargs)

    def get_paste(self, paste: str) -> Optional[str]:
        if "https://pastebin.com" in paste:
            return None
        paste_key = self.get_paste_key(paste)
        if not paste_key:
            return None
        paste_meta = self.get_paste_metadata_by_key(paste_key)
        paste_meta["content"] = self.get_paste_by_key(paste_key)
        return paste_meta

    def get_paste_by_key(self, paste_key: str) -> str:
        return self.get(f"api_scrape_item.php?i={paste_key}").text

    def get_paste_metadata_by_key(self, paste_key: str) -> dict:
        res = self.get(f"api_scrape_item_meta.php?i={paste_key}")
        if res.text[0:5] == "Error" or res.text[0:6] == "Please":
            return {"error": res.text}
        try:
            res = res.json()
        except JSONDecodeError:
            return {"error": res.text}
        return res

    def get_paste_key(self, paste_link: str) -> Optional[str]:
        paste_key = ""
        if "pastebin.com" in paste_link:
            pattern = r'https://pastebin.com/([A-Za-z0-9]{8,})'
            try:
                paste_key = re.findall(pattern, paste_link)[0]
            except IndexError:
                self.log.warning(f"No paste key found in: {paste_link}")
                return None
        return paste_key or paste_link

    def request(self, *args, **kwargs):
        return self._request(*args, **kwargs)
