import logging

from typing import Optional

class Template(object):
    """ Provides bootstrapping methods for gearbox cogs """
    def __init__(self, global_config: dict = None, testing: bool = False, *args, **kwargs):
        """
        Performs safe initialization activities

        Classes inheriting from this that define an `__init__` method should
        always call super().__init__(*args, **kwargs) before any other action.
        """
        self.global_config = global_config or {}
        if self.global_config.get("test"):
            import doctest
            flags = doctest.ELLIPSIS
            doctest.testmod(
                optionflags=flags,
                verbose=self.global_config.get("verbose")
            )
        # Doctests don't receive the same context, so instances
        # need another flag to tell them they're in test mode.
        self.testing = testing
        self.log = logging.getLogger(self.__class__.__name__)

    def setup(self, *args, **kwargs) -> Optional[bool]:
        """
        Returns True if no terrible things happened setting up

        Classes inheriting from this that define a setup() method should always
        call super().setup(*args, **kwargs) and only proceed with setting up
        if the response is truthy.
        """
        if not hasattr(self, "testing"):
            self.log.error("Error - setup called but class not initialized!")
            return False
        if not self.testing:
            return True
        return None
        # No return prevents cogs from running which is useful for running tests
