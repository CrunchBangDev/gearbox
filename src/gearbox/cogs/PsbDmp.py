from datetime import datetime, timedelta

from gearbox.models import PasteLink
from gearbox.cogs.templates import APIClient


class PsbDmp(APIClient):
    schedule = 300000
    api_date_format = "%d.%m.%Y"
    emits = PasteLink

    def main(self, interrupt):
        dumps = 0
        date_from = (datetime.now() - timedelta(days=1)).strftime(self.api_date_format)
        date_to = datetime.now().strftime(self.api_date_format)
        res = self.post(f"getbydate", data={"from": date_from, "to": date_to})
        if not res:
            return
        for paste in res[0]:
            if "pass" not in paste["tags"]:
                continue
            self.emit(PasteLink(**paste))
            dumps += 1
        if dumps:
            self.log.info(f"Found {dumps} dump{'' if dumps == 1 else 's'}")
