import functools
import gzip
import hashlib
import html.parser
import logging
import operator
import os
import re
import shutil
import tarfile
import urllib.parse
import zipfile
from email.mime.text import MIMEText
from functools import reduce
from os.path import dirname
from typing import List, Optional, Tuple, Union


def merge(*iterables: list) -> set:
    """
    Return set of unique items from any number of iterables
    Iterables with unhashable contents will raise a TypeError

    Parameters:
        *iterables (list): any number of hashable iterables
    """
    return set(reduce(operator.or_, map(set, iterables)))

def diff(*iterables: list) -> set:
    """
    Return list of unique items which appear once in any number of iterables
    Iterables with unhashable contents will raise a TypeError

    Parameters:
        *iterables (list): any number of hashable iterables
    """
    return set(reduce(operator.sub, map(set, iterables)))

def dedup(payload: list) -> list:
    """ Remove duplicates from a single iterable """
    return list(set(payload))

def parent_dir(filepath: str = None) -> str:
    """
    Return the parent directory for a given filepath
    
    Given the filepath /foo/bar/baz/bat.txt, this function returns `/foo/bar`
    """
    file = filepath or __file__
    return dirname(dirname(file))

def splitext(fname: str, extlim: int = -1, extpartlim: int = None) -> Tuple[str, str]:
    """
    Returns name and extension parts for a given filename.
    Basically os.path.splitext() except recursive and with options.

    Parameters:
        fname (str): The filename to split
        extlim (int): Limit the number of characters allowed in an extension
        extpartlim (int): Limit the number of extension parts returned
    """
    ext = ""
    extpartlim = extpartlim or fname.count(os.extsep)
    if extlim >= len(fname):
        extlim = -1
    while os.extsep in fname[-extlim:] and ext.count(os.extsep) < extpartlim:
        fname, ext2 = os.path.splitext(fname)
        if not ext2:
            break
        ext = ext2 + ext
    return fname, ext

def unique_fname(filename: str, directory: str) -> (str):
    """
    Determines a unique filename to avoid overwriting existing files

    Parameters:
        filename (str): The desired filename
        directory (str): Filesystem location at which to check for conflicts
    """
    basename, ext = splitext(filename)
    if not os.path.exists(os.path.join(directory, filename)):
        return filename
    i = 1
    while os.path.exists(os.path.join(directory, f"{basename} ({i}){ext}")):
        i += 1
    # Apply the filename determined by the previous step
    return f"{basename} ({i}){ext}"

def sha256(filename: str) -> Optional[str]:
    """
    Calculates the sha256 hash of a given file on disk.

    Parameters:
        fname (str): Path to the file to be hashed
    """
    sha2h = hashlib.sha256()
    with open(filename, "rb") as f:
        for chunk in iter(functools.partial(f.read, 256), b""):
            sha2h.update(chunk)
    return sha2h.hexdigest()

def hash_message(message: object) -> str:
    return hashlib.sha224(message.idempotency_key.encode("utf-8")).hexdigest()

def extract_file(fname: str, sDir: str, tDir: str = "", loop: bool = True) -> List:
    """
    Extracts various archival and compression formats, returns a list of files

    Parameters:
      fname (str): File to be extracted
      sDir (str): Location where file is stored
      tDir (str): Location to extract files to
      loop (bool): Whether to recursively extract child archives
    """
    tDir = tDir or sDir
    os.makedirs(tDir)
    filepath = os.path.join(sDir, fname)
    f_base, f_ext = os.path.splitext(fname)

    extractors = {
        ".zip": extract_zip_file,
        ".gz": extract_gzip_file,
        ".gzip": extract_gzip_file,
        ".tar": extract_tar_file,
    }

    # Unsupported type or not a compressed/archived file
    if f_ext not in extractors.keys():
      return [fname]
    extracted_files = extractors[f_ext](filepath, tDir, f_base)  # type: ignore
    os.remove(filepath)
    if not loop:
      return extracted_files
    # Iterate back through, in case of layered archives
    # or compressed archives (e.g. example.tar.gz)
    final_extracted_files = []
    for fname in extracted_files:
      # Set loop switch to False to avoid creating blackhole
      final_extracted_files.extend(extract_file(fname, tDir, loop=False))
    return final_extracted_files

def extract_zip_file(filepath: str, tDir: str, *args) -> Union[list, bool]:
    extracted_files = []
    with zipfile.ZipFile(filepath) as f:
        # testzip() returns None or name of first bad file
        e = zipfile.ZipFile.testzip(f)
        if e is not None:
            raise(Exception(e))
        # Not using extractall() because we don't want a tree structure
        for member in f.infolist():
            member_info = unique_fname(str(member), tDir)
            f.extract(member_info, tDir)
            extracted_files.append(str(member_info))
    return extracted_files

def extract_gzip_file(filepath: str, tDir: str, f_base: str) -> list:
    out_fname = unique_fname(f_base, tDir)
    outfile = os.path.join(tDir, out_fname)
    with gzip.open(filepath, "rb") as f_in, open(outfile, "wb") as f_out:
        shutil.copyfileobj(f_in, f_out)
    return [str(out_fname)]

def extract_tar_file(filepath: str, tDir: str, *args) -> list:
    extracted_files = []
    with tarfile.open(filepath, "r") as tar:
        for member in tar.getmembers():
            if not member.isreg():
                continue
            # Strip any path information from members
            member.name = unique_fname(
                os.path.basename(member.name),
                tDir
            )
            tar.extract(member, tDir)
            extracted_files.append(str(member.name))
    return extracted_files
