from django.db import models
from uuid import uuid4

class ErrorTracker(models.Model):
    cog = models.CharField(max_length=128, primary_key=True)
    disabled = models.BooleanField(default=False)
    error_status = models.BooleanField(default=False)
    errors = models.IntegerField(default=0)
    
    @property
    def idempotency_key(self):
        # UUID4 ensures messages of this type will always be processed
        return f"{self.__class__.__name__}_{self.cog}_{uuid4()}"

class TimeKeeper(models.Model):
    cog = models.CharField(max_length=128, primary_key=True)
    run_time = models.IntegerField(default=0)
    times_run = models.IntegerField(default=0)
    avg_run_time = models.IntegerField(default=0)
    longest_run = models.IntegerField(default=0)
    
    @property
    def idempotency_key(self):
        return f"{self.__class__.__name__}_{self.cog}_{self.times_run}"

class Scheduler(models.Model):
    cog = models.CharField(max_length=128, primary_key=True)
    last_run = models.DateTimeField(auto_now=True)
    
    @property
    def idempotency_key(self):
        return f"{self.__class__.__name__}_{self.cog}_{self.last_run}"
