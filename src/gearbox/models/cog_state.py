from django.db import models

class CogMemory(models.Model):
    cog = models.CharField(max_length=128)
    message_id = models.CharField(max_length=56)
    recorded = models.DateTimeField(auto_now=True)

    @property
    def idempotency_key(self):
        return f"{self.cog}_{self.message_id}"
