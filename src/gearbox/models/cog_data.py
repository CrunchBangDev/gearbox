from django.db import models


class PasteLink(models.Model):
    id = models.CharField(max_length=10, primary_key=True)
    tags = models.TextField()
    date = models.TextField()
    recorded = models.DateTimeField(auto_now=True)

    @property
    def idempotency_key(self):
        return f"{self.__class__.__name__}_{self.id}"

class Paste(models.Model):
    key = models.CharField(max_length=10, primary_key=True)
    title = models.TextField()
    content = models.TextField()
    syntax = models.TextField()
    user = models.TextField()
    date = models.TextField()
    expire = models.TextField()
    recorded = models.DateTimeField(auto_now=True)

    @property
    def idempotency_key(self):
        return f"{self.__class__.__name__}_{self.key}"
