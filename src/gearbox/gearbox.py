import time

init_time = time.time()

import glob
import inspect
import os
import sys
from datetime import timedelta
from importlib.abc import Loader
from importlib.util import module_from_spec, spec_from_file_location
from os.path import basename, isfile, join, splitext
from pubsub import pub
from threading import Event
from typing import Dict, List, Optional, Type

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'gearbox.settings')

import django

django.setup()

from . import settings
from .cogs.templates.Cog import Cog
from .manager import Manager
from .models import CogMemory
from .utils import merge, parent_dir

class GearBox(Manager):
    """ Core class definition for gearbox framework """
    # There are a few categories of utility meta cogs can provide
    # but for now only managers are implemented
    meta_types = ("manager", )
    cogs: List[Type[Cog]] = []
    meta_cogs: Dict[str, List[Type[Cog]]] = {}
    # Interrupts allow us to ask cogs nicely to return
    interrupts: List[Event] = []
    def __init__(self, protection = None, **kwargs):
        if protection:
            self.protection = protection
        super().__init__(**kwargs)

    def main(self):
        """ Loads, initializes, and executes cogs """
        # Find, load, and initialize meta cogs
        meta_config: dict = self.global_config.get("meta", {})
        default_meta_path = join(parent_dir(), "gearbox", "meta")
        meta_cogs_path = self.global_config.get("meta_path", default_meta_path)
        for meta_type in self.meta_types:
            meta_cog_names: List[str] = meta_config.get(meta_type, [])
            meta_cogs = self.get_available_cogs(join(meta_cogs_path, meta_type))
            if not meta_cogs:
                self.log.debug(f"No {meta_type} cogs at path: " +
                               join(meta_cogs_path, meta_type))
                continue
            # If meta_cog_names is empty, all available cogs will be loaded!!!
            loaded_meta_cogs: List[Type[Cog]] = \
                    self.load_cogs(meta_cogs, meta_cog_names)
            initialized_meta_cogs: List[Type[Cog]] = \
                    self.initialize_cogs(loaded_meta_cogs, self.global_config)
            if not initialized_meta_cogs:
                self.log.error(f"Failed to load {meta_type} cog(s)")
                continue
            self.meta_cogs[meta_type] = initialized_meta_cogs

        # Warn about missing capabilities
        if not self.global_config.get("no-warn", False) and not self.testing:
            for capability in self.meta_types:
                if not self.meta_cogs.get(capability, False):
                    self.warn_missing_capability(capability, "loaded")

        self.log.debug(f"Meta cog init completed in {round(time.time() - init_time, 6)}s")
        # Get a list of available cogs, optionally specifying a path
        default_cog_path = join(parent_dir(), "gearbox", "cogs")
        path = self.global_config.get("path", default_cog_path)
        cog_paths: List[str] = self.get_available_cogs(path)
        self.log.debug(f"Found {len(cog_paths)} cog(s)")

        # Get list of desired cogs (if specified)
        wanted = [x.lower() for x in self.global_config.get("cogs", []) if x]
        # Load all [wanted] cogs
        loaded_cogs: List[Type[Cog]] = self.load_cogs(cog_paths, wanted)
        self.log.info(f"Loaded {len(loaded_cogs)} cog(s)")
        # Pretty print loaded cogs (if requested)
        if self.global_config.get("show-cogs", False):
            nl = os.linesep
            assert(isinstance(self.cogs, list))  # mypy
            cog_names = nl.join(f"    {x.__name__}" for x in loaded_cogs)            
            meta_cog_names = nl.join(
                f"  {k.title()}: {', '.join(x.name for x in v)}"
                for k, v in self.meta_cogs.items() if v
            )
            if loaded_cogs:
                print(f"Cogs:{nl}{cog_names}{nl}")
            if meta_cog_names:
                print(f"Meta cogs:{nl}{meta_cog_names}{nl}")
        else:
            # Report init time
            load_time = time.time()
            self.log.debug(f"Init completed in {round(load_time - init_time, 6)}s")
            # Initialize cogs with configuration
            self.cogs = self.initialize_cogs(loaded_cogs, self.global_config)
            self.publish_historical_payloads(self.cogs)
            self.log.debug(f"Cog init completed in {round(time.time() - load_time, 6)}s")
            # Spawn cog worker threads or delegate to manager cogs
            self.run_or_delegate_cogs(self.cogs)
        if getattr(self, "protection"):
            self.protection.cleanup()

    def warn_missing_capability(self, capability: str, stage: str):
        """ Warn about missing meta cog capabilities """
        capability_warnings = {
            "Secrets": "Remote secrets will not be available!",
            "Manager": "Cogs will have no defined context!",
            "Message": "Cogs will be unable to send messages!",
        }
        response = capability_warnings.get(capability.title(),
                                           "No one knows what might happen!")
        self.log.warn(f"No {capability.lower()} cog {stage}! {response}")

    def get_available_cogs(self, cog_path: str = "cogs") -> List[str]:
        """
        Get cogs from a given directory, or an adjacent 'cogs' directory

        Args:
            cog_path (str, optional): A directory containing cogs. Default "../cogs"

        Returns:
            List[str]: A list of absolute cog paths
        """
        try:
            # Calling dirname twice gets the parent directory
            return glob.glob(join(cog_path, "*.py"))
        except Exception as e:
            self.log.exception(f"Failed to get cogs from path: {cog_path}",
                               exc_info=e)
            return []

    def load_cogs(self, cogs: List[str], wanted_cogs: List[str] = None) -> List[Type[Cog]]:
        """
        Loads a list of cog modules given their full paths
        """
        wanted_cogs = wanted_cogs or []
        loaded_cogs: List[Type[Cog]] = []
        # Iterate through the list of cog filepaths
        for cog_path in cogs:
            cog_name, _ = splitext(basename(cog_path))
            if wanted_cogs:
                try:
                    wanted_cogs.remove(cog_name.lower())
                except ValueError:
                    # Cog was not on wanted list - silently skip it
                    continue
            try:
                cog = self.load_cog(cog_path)
            except Exception as e:
                self.log.exception(f"Failed to load cog: {cog_path}", exc_info=e)
                continue
            if cog:
                loaded_cogs.append(cog)
        # Warn about any wanted cogs that weren't found
        if wanted_cogs:
            self.log.warning("Cog(s) not found: " + ", ".join(wanted_cogs))
        return loaded_cogs

    def load_cog(self, cog_path: str) -> Optional[Type[Cog]]:
        """
        Loads a cog module from a file and executes the top level
        """
        # Import the cog
        cog_name, _ = splitext(basename(cog_path))
        spec = spec_from_file_location(cog_name, cog_path)
        assert(isinstance(spec.loader, Loader))
        cog = module_from_spec(spec)
        spec.loader.exec_module(cog)

        # Search module for class to initialize
        for member_name, member in inspect.getmembers(cog):
            if member_name.lower() == cog_name.lower() and inspect.isclass(member):
                return member
        raise Exception(f"No usable class found in module '{cog_name}'")

    def initialize_cogs(self, cogs: List[Type[Cog]], config: dict) -> List[Type[Cog]]:
        """
        Executes the `__init__` and `setup` methods of the cog class
        """
        initialized_cogs: List[Type[Cog]] = []
        for cog in cogs:
            try:
                init_cog = cog(global_config=config)
            except Exception as e:
                self.log.exception(f"Failed to initialize '{cog.__name__}'",
                                   exc_info=e)
                continue
            setup_result = init_cog.setup()
            if setup_result is True:
                init_cog.log.debug("Cog initialized")
                # Give cog a REFERENCE to (not a copy of!) the list of meta cogs
                # This way meta cogs loaded early (e.g. secrets cogs) get full
                # knowledge of all meta cogs once the setup phase is complete
                init_cog.meta_cogs = self.meta_cogs
                initialized_cogs.append(init_cog)
            else:
                # Exception may be stored in cog config if it occurred during init
                error = setup_result or init_cog.config.get('error')
                self.log.exception(f"Cog '{cog.__name__}' setup failed: {error}")
                continue
            # Load cog state information
            if hasattr(cog, "state_model"):
                for record in cog.state_model.objects.values():
                    init_cog.history[record["cog"]] = init_cog.state_model(**record)
            else:
                try:
                    latest_record = CogMemory.objects.filter(cog=init_cog.name).latest('recorded').recorded
                except CogMemory.DoesNotExist:
                    continue
                init_cog.messages_seen = \
                    list(CogMemory.objects.filter(cog=init_cog.name).filter(recorded__gte=latest_record - timedelta(days=1)).values_list('message_id', flat=True))
        return initialized_cogs
    
    def publish_historical_payloads(self, cogs: List[Type[Cog]]) -> None:
        for cog in cogs:
            if not cog.emits:
                continue
            if isinstance(cog.emits, tuple):
                for topic in cog.emits:
                    self.publish_historical_payloads_for_topic(topic)
            else:
                self.publish_historical_payloads_for_topic(cog.emits)
                
    def publish_historical_payloads_for_topic(self, topic) -> None:
        try:
            latest_record = topic.objects.latest('recorded').recorded
        except topic.DoesNotExist:
            return None
        for payload in topic.objects.filter(recorded__gte=latest_record - timedelta(days=1)):
            topic_name = topic().__class__.__name__.lower()
            pub.sendMessage(topicName=topic_name, payload=payload)
