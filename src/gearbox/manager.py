import asyncio
from concurrent.futures import (FIRST_COMPLETED, ThreadPoolExecutor,
                                as_completed, wait)
from threading import Event
from time import sleep
from types import MethodType
from typing import Dict, List, Optional, Tuple, Type, Union

from .cogs.templates.Cog import Cog
from .cogs.templates.template import Template
from .utils import diff


class Job:
    worker: Type[Cog]
    managers: List[Type[Cog]]
    interrupt: Event
    def __init__(self,
                 worker: Type[Cog],
                 managers: List[Type[Cog]],
                 interrupt: Event):
        self.worker = worker
        self.managers = managers
        self.interrupt = interrupt


class Manager(Template):
    """ Base class for any cog that will run other cogs """
    # Cogs are life
    cogs: List[Type[Cog]] = []
    # Meta cogs provide various utilities to other cogs
    meta_cogs: Dict[str, List[Type[Cog]]] = {}
    # Interrupts allow us to ask cogs nicely to return
    interrupts: List[Event] = []

    def run_or_delegate_cogs(self, cogs: List[Type[Cog]], parents: List[Type[Cog]] = None) -> List[Type[Cog]]:
        """
        Sends cogs to the appropriate manager, either `self.run_cogs` or the
        first available manager.
        """
        parent_managers = parents or []

        # Look for managers to delegate to
        manager_cogs = self.meta_cogs.get("manager",[])
        # Avoid singularity creation caused by recursive self-delegation
        parent_managers.append(self)
        for manager in manager_cogs:
            if manager in parent_managers:
                continue
            # Found free manager, delegate to it
            return manager.run_or_delegate_cogs(cogs, parent_managers)

        # No one to delegate to, we'll have to take matters into our own hands
        return self.run_cogs(cogs, parent_managers)

    def run_cogs(self, cogs: List[Type[Cog]], managers: List[Type[Cog]]):
        """
        Implements an asynchronous thread pool for cogs to run in
        
        When `spawn_worker` returns a cog that has finished its work, it is
        added back into the pool. The loop waits for only one thread to complete
        before updating the futures list and waiting again. An empty futures list
        will terminate the pool.
        """
        if not cogs:
            return
        with ThreadPoolExecutor() as pool:
            futures = [pool.submit(self.spawn_worker, cog, managers.copy()) for cog in cogs]
            try:
                while futures:
                    # Get latest finished future - mypy hates this for some reason
                    finished = wait(futures, return_when=FIRST_COMPLETED).done.pop()  # type: ignore
                    # Remove this future from our tracking list
                    futures.remove(finished)
                    cog: Optional[Type[Cog]] = finished.result()
                    if not cog:
                        # Cog must have crashed, do not re-queue it
                        # TODO: Maybe find out what happened to it
                        continue
                    # No need to restack managers since cog is presumably already bootstrapped
                    worker = pool.submit(self.spawn_worker, cog, managers.copy())
                    futures.append(worker)
            except KeyboardInterrupt:
                print()
                self.log.debug("Received keyboard interrupt - terminating job queue")
            # Ask cogs to return
            for interrupt in self.interrupts:
                interrupt.set()
            pool.shutdown()
            # Pool is closed

    def spawn_worker(self, cog: Type[Cog], managers: List[Type[Cog]]) -> Optional[Type[Cog]]:
        interrupt = Event()
        self.interrupts.append(interrupt)
        try:
            job = Job(cog, managers, interrupt)
            result = self.run_or_delegate_job(job)
            if result is False:
                return result
            return cog
        except Exception as e:
            self.log.exception(f"Cog '{cog.__class__.__name__}' crashed",
                               exc_info=e)
            return None

    def run_or_delegate_job(self, job: Job) -> Optional[Job]:
        """ Delegate to next available job manager, or run job directly """
        job.managers.remove(self)
        if job.managers:
            return job.managers[-1].run_or_delegate_job(job)
        else:
            return self.run_job(job)

    def run_job(self, job: Job) -> Optional[Job]:
        """ Run cog's `main` method, passing in the job's interrupt event """
        result = job.worker.main(job.interrupt)
        # Return of False means do not run me again
        if result is False:
            return result
        return job

    def get_cog_state(self, cog: Cog) -> object:
        """ Load state information for a given cog from manager's state info """
        return self.history.get(cog.name, self.state_model(cog=cog.name))
