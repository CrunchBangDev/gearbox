import logging
import os
import sys
import time
from logging.handlers import TimedRotatingFileHandler

# Gitlab dependencies
from protection import protect  # type: ignore

from .gearbox import GearBox  # type: ignore
from .gearbox_settings import app, version, config

log_config = {
    "format": "[%(name)s:%(levelname)s] %(message)s",
    "level": getattr(logging, config["log-level"].upper()) or logging.INFO,
}
if config["log"]:
    if not os.path.exists("log"):
        os.mkdir("log")
    logfile = os.path.join("log", f"{app}.log")
    handler_config = {"filename": logfile,
                      "when": "midnight",
                      "backupCount": 7}
    # mypy really hates when you send kwargs this was
    log_config["handlers"] = [TimedRotatingFileHandler(**handler_config)]  # type: ignore
logging.basicConfig(**log_config)  # type: ignore


p = protect(name=app, max_age=600)
gearbox = GearBox(global_config=config, protection=p)
gearbox.log.info(f"Started {app} v{'.'.join(str(x) for x in version)}")
gearbox.main()
