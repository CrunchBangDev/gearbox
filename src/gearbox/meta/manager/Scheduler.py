from datetime import datetime, timedelta, timezone
from threading import Event
from time import sleep
from typing import List, Optional, Tuple, Type, Union

from gearbox.models import Scheduler as SchedulerModel
from gearbox.cogs.templates import Cog
from gearbox.manager import Job, Manager
from gearbox.utils import merge


class Scheduler(Cog, Manager):
    """ Checks cog schedules, if defined, to determine if they should run """
    state_model = SchedulerModel
    def setup(self, *args, **kwargs):
        self.started_at = datetime.now(timezone.utc)
        return super().setup(*args, **kwargs)

    def run_or_delegate_job(self, job: Type[Job]) -> Optional[Type[Job]]:
        cog = job.worker
        state = self.get_cog_state(cog)
        # On first run, set tiny timeout to ensure the cog runs
        if not state.last_run:
            last_run = self.started_at
            schedule = 1
        else:
            last_run = state.last_run
            schedule = getattr(cog, "schedule", None)
        cog_is_scheduled, backoff = self.is_scheduled(schedule, last_run)
        if not cog_is_scheduled:
            # If no backoff is returned, cog is never scheduled
            if backoff is False:
                # cog.log.debug(f"Cog is never scheduled to run")
                # Returning False signals not to requeue this job
                return False
            cog.log.debug(f"Waiting {backoff}s to run")
            # Don't return until scheduled or interrupted
            while not job.interrupt.is_set():
                job.interrupt.wait(backoff)
                break
            return None
        # Save before running to ensure timestamp is accurate and not late
        state.save()
        result = super().run_or_delegate_job(job)
        if result:
            self.history[cog.name] = state
            return job
        return result

    def is_scheduled(self, schedule: int, last_run):
        if schedule is False:
            return False, False
        schedule = schedule or 1000
        sched = timedelta(milliseconds=schedule)
        backoff = (last_run + sched - datetime.now(timezone.utc)).total_seconds()
        return backoff < 0, backoff
