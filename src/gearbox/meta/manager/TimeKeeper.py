import time
from datetime import datetime, timedelta
from typing import List, Optional, Tuple, Type

from gearbox.cogs.templates.Cog import Cog
from gearbox.manager import Job, Manager
from gearbox.models import TimeKeeper as TimeKeeperModel
from gearbox.utils import merge


class TimeKeeper(Cog, Manager):
    """Runs other cogs and tracks their performance statistics and error states"""
    state_model = TimeKeeperModel
    def run_or_delegate_job(self, job: Type[Job]) -> Optional[Type[Job]]:
        cog = job.worker
        state = self.get_cog_state(cog)
        cog_start = time.time()
        result = super().run_or_delegate_job(job)
        if not result:
            return result
        cog_end = time.time()
        state.run_time = cog_end - cog_start
        self.log.debug(f"Ran cog {cog.name} in {round(state.run_time, 6)}s")
        state.times_run = state.times_run + 1
        avg_t = float(state.avg_run_time)
        state.avg_run_time = avg_t + (state.run_time - avg_t) / state.times_run
        if state.run_time > float(state.longest_run):
            state.longest_run = state.run_time
        self.history[cog.name] = state
        state.save()
        return job
