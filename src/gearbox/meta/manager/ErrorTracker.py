import time
from datetime import datetime, timedelta
from typing import List, Optional, Tuple, Type

from gearbox.cogs.templates.Cog import Cog
from gearbox.manager import Job, Manager
from gearbox.models import ErrorTracker as ErrorTrackerModel
from gearbox.utils import merge

module_config = ["run_with_errors"]


class ErrorTracker(Cog, Manager):
    """Runs other cogs and tracks their performance statistics and error states"""
    state_model = ErrorTrackerModel
    def __init__(self, *args, config=module_config, **kwargs):
        super().__init__(*args, config=merge(config, module_config), **kwargs)

    def run_or_delegate_job(self, job: Type[Job]) -> Optional[Type[Job]]:
        cog = job.worker
        state = self.get_cog_state(cog)
        if state.disabled:
            self.log.warn(f"{cog.name} is disabled.")
            return False
        elif state.error_status and not self.config["run_with_errors"]:
            self.log.error(f"{cog.name} will not be run due to error status.")
            return False
        result = super().run_or_delegate_job(job)
        if result:
            return job
        # Notify if error will result in indefinite suspension of cog
        if not self.config.get("run_with_errors"):
            self.log.error(f"{cog.name} encountered an error and will " +
                            "not run again until cleared.")
        state.error_status = True
        state.errors += 1
        self.history[cog.name] = state
        state.save()
        return result
