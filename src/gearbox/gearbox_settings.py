import argparse
import os
import sys
import textwrap

from conf import Conf  # type: ignore
from conf import ConfItem as CI  # type: ignore

app = "gearbox"
version = (0, 1, 0, 0)  # major, minor, micro, patch
description = "A python automation framework"

abstract_conf = (
  CI("--log",
     "this will record all output to a file", bool),
  CI("--show-cogs",
     "list available cogs and exit", bool),
  CI("--no-warn",
     "do not warning about missing meta cogs", bool),
  CI("-cogs",
     "specify a list of cogs to run", str),
#   CI("path",
#      "specify a path from which to load cogs", str,),
  CI("log-level",
     "specify a log verbosity level", str, "INFO",
     allowed=("DEBUG, INFO, WARNING, ERROR, CRITICAL")),
  CI("date_format",
     "specify a date format", str, "%Y-%m-%d"),
  CI("datetime_format",
     "specify a datetime format", str, "%Y-%m-%d %H:%M:%S"),
  CI("meta",
     "dictionary of meta providers (not commandline friendly)", dict)
)

config = Conf(prog=app,
              version=".".join(str(x) for x in version),
              abstract_conf=abstract_conf,
              description=description).config
