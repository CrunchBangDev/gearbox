# gearbox

Do all the things!

## Intro

Gearbox is a framework and engine in which to run arbitrary logical widgets called cogs. Data is moved between cogs in a publisher-subscriber model powered by PyPubSub and is structured around data models powered by Django's ORM. A published model is called a topic and cogs can subscribe to it. A database cog subscribes to all topics to provide storage for any data that other cogs may emit. Subscriber cogs are aware of the data they've processed before and will avoid repeating work, even across application restarts.

## Setup

```sh
# Download the project
git clone https://gitlab.com/CrunchBangDev/gearbox
cd gearbox
# Install dependencies
pip install -r requirements.txt
cd src
# Set up the database
python manage.py makemigrations gearbox
python manage.py migrate
# List the available cogs
python -m gearbox --show-cogs
```

The output should look something like this:

```
[GearBox:INFO] Started gearbox v0.1.0.0
[GearBox:INFO] Loaded 3 cog(s)
Cogs:
    PastebinFetch
    Database
    PsbDmp

Meta cogs:
  Manager: ErrorTracker, Scheduler, TimeKeeper
```

Note that, as this project is an early work in progress, the options displayed by `python -m gearbox --help` may not all work.

You should now be ready to run gearbox!

## Getting Started

The included cogs require some configuration before they can run, as gearbox will happily tell you if you try to run it now:

```
[GearBox:INFO] Started gearbox v0.1.0.0
[GearBox:INFO] Loaded 3 cog(s)
[GearBox:ERROR] Cog 'PastebinFetch' setup failed: Missing config element(s): url, api_key
[GearBox:ERROR] Cog 'PsbDmp' setup failed: Missing config element(s): url
```

It is left as an exercise to the reader to determine and populate the appropriate values in the `cog_config.yml` file. Note that configuration values are prefixed with the cog (or cog template) they apply to: to set the `url` for the `Foo` cog, you must set a value for `Foo_url` in the cog config. The provided config contains one example.

## Why Doesn't It Work?

There could be so many reasons, but primarily it's because this project is nowhere near finished.

## How Can I Write My Own Cog?

Easy! Create a new file in the `gearbox/cogs` folder and name it something decriptive like `ThingThatDoesImportantStuff.py`. In that file, create a class and name it the same way, inheriting from the `Cog` template.

```python
from gearbox.cogs.templates import Cog

class ThingThatDoesImportantStuff(Cog):
    pass
```

Now you have to make some decisions - what will the cog do? If it publishes data for other cogs to consume, we'll need to give it an `emits` property with the models it can emit. Similarly if it subscribes to data from other cogs, we'll need to give it a `collects` property. Let's say our cog collects `Foo`s and emits `Bar`s and `Bat`s - our cog would then look like this:

```python
from gearbox.cogs.templates import Cog
from gearbox.models import Foo, Bar, Bat

class ThingThatDoesImportantStuff(Cog):
    collects = Foo
    # We can group multiple models in a tuple
    emits = (Bar, Bat)
```

Now we just need to write the logic for how to produce `Bar`s and `Bat`s and what to do when we gets `Foo`s. We can define those as the `main` and `receive` methods, respectively:

```python
    # The interupt allows cogs to shutdown gracefully
    # It can also be used to sleep without blocking execution
    def main(self, interrupt):
        # Produce some data to publish
        some_data = {"hello": "world"}
        # Package the data using the appropriate model
        payload = Bar(**some_data)
        # Ship it!
        self.emit(payload)
        # You could even do everything in one line
        self.emit(Bat("what_kind_of_bat": "the example kind, I guess"))
    
    def receive(self, payload):
        # Payloads are populated Django ORM models
        print(f"Foo received: {payload.something_a_foo_has}")
```

We'll also need to create the models being used here (add them to `gearbox/models/cog_data.py`):

```python
class Foo(models.Model):
    something_a_foo_has = models.TextField()

class Bar(models.Model):
    hello = models.TextField()

class Bat(models.Model):
    what_kind_of_bat = models.TextField()
```

You'll also want to make sure the models are imported in `gearbox/models/__init__.py`. Don't forget to make and run migrations any time you update models.

Congratulations! You now have a cog! Now try to make another one that emits `Foo`s and/or collects `Bar`s or `Bat`s.

## References

Documentation for libraries this project makes heavy use of:

 - [PyPubSub](https://pypubsub.readthedocs.io/en/stable/)
 - [Django](https://django.readthedocs.io/en/stable/)

## Disclaimer

This software was developed solely for my personal education and does not have any express purpose. The provided cogs are meant only to serve as examples and inspiration. Please do not use this software for any illegal, unethical, immoral, dangerous, reprehensible, uncool, vaguely sketchy, or other purposes.
